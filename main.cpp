#include <iostream>
#include <graphics.h>
#include <stdlib.h>
#include <time.h>
#include <string>
#include <vector>
#include <fstream>

#define max_x 800
#define max_y 600
#define max_tam (max_x-20)/20*(max_y-20)/20
#define UP 72
#define DOWN 80
#define LEFT 75
#define RIGHT 77
#define SPACE 32
#define ESC 27
#define ever (;;)

using namespace std;

const char *game_over = "game_over.bmp";
const char *head_up = "head_up.bmp";
const char *head_down = "head_down.bmp";
const char *head_left = "head_left.bmp";
const char *head_right = "head_right.bmp";
const char *corpo = "corpo.bmp";
const char *comida = "maca.bmp";
const char *borda = "borda.bmp";
const char *tela = "tela.bmp";

void ponto();//imprime a pontua��o nova  a tela
void coloca_comida();//coloca o bitmap da ma�a em um lugar aleatorio da tela
void recoloca_comida();//coloca o bitmap da maca novamente
void gameover();//coloca o bitmap gameover na tela e encerra o programa
void bordas(); //coloca as bordas na tela
void saveHighscore();//salva a pontua��o e imprime as 10 maiores
bool verifica_pos();//retorna falso se a posi��o atual for inv�lida

struct cobra
{
	int x;
	int y;
};

short x = max_x/2, y=max_y/2, dir = 0,
      seta=0, x_comida, y_comida, speed=0;

string nome;

vector<cobra> snake;

string toString(int numInt)
{
    string numStr;
    int i=10, j=1;

    if(numInt < 0) {
        numStr.push_back('-');
        numInt = -numInt;
    }

    while(numInt%i != numInt) {i *= 10;}

    while(i>1)
    {
        i/=10;
        numStr.push_back(48+(numInt/i));
        numInt -= (numInt/i)*i;
    }

    return numStr;
}

int toInt(string numStr)
{
    int numInt=0, n=1;

    for(int i=0; i<numStr.size(); i++, n*=10);

    for(int i=0; i<numStr.size(); i++)
    {
        n/=10;
        numInt += (numStr[i]-48)*n;
    }

    return numInt;
}

void saveHighscore()
{
    ifstream file;
    ofstream out;
	string line;
	int score = (snake.size()-2)*10*speed;
	bool flag=false;

	struct{
		string nome;
		string pontos;
	}arquivo[128], aux;

	file.open("highscores.txt");

	for(int i=0; i<100; i++)
	{
        getline(file,arquivo[i].nome,',');//copia a string ate achar ,
        getline(file,arquivo[i].pontos);//copia a string ate achar uma linha nova

        if(arquivo[i].nome.compare(nome) == 0)//verifica se ja existe o nome
        {
            flag = true;
            if(toInt(arquivo[i].pontos) < score)
                arquivo[i].pontos = toString(score);
        }
	}

	file.close();

    if(!flag)//caso o nome ainda nao exista no arquivo
    {
        arquivo[100].nome = nome;
        arquivo[100].pontos = toString(score);
    }
    else
    {
        arquivo[100].nome = "0";
        arquivo[100].pontos = "0";
    }

	for(int i=0; i<=100; i++)//bubblesort, y not ? hahaha
        for(int j=0; j<100; j++)
            if(toInt(arquivo[j].pontos) < toInt(arquivo[j+1].pontos))
            {
                aux = arquivo[j];
                arquivo[j] = arquivo[j+1];
                arquivo[j+1] = aux;
            }

    cout << "Top 10 Highscores:\n" ;

    for(int i=0; i<10; i++)
        cout << arquivo[i].nome << " " << arquivo[i].pontos << endl;

    cout << endl;

    out.open("highscores.txt");

    for(int i=0; i<100; i++)
        out << arquivo[i].nome << "," << arquivo[i].pontos << endl;

    out.close();
}

bool verifica_pos()
{
	if(x<20 || y<20 || x>max_x-40 || y>max_y-40) return false;

	for(int i=2; i<snake.size(); i++)
		if(snake[i].x == x && snake[i].y == y) return false;

	return true;
}

void coloca_comida()
{
	srand(time(NULL));

    for ever
    {
        x_comida = (rand() % max_x-20) + 20;
        y_comida = (rand() % max_y-20) + 20;

        if(x_comida<20 || y_comida<20 || x_comida>max_x-40 || y_comida>max_y-40) continue;

        if(x_comida % 20 == 0 && y_comida % 20 == 0 && x != x_comida && y != y_comida)
        {
            readimagefile(comida, x_comida, y_comida, x_comida+20, y_comida+20);
            break;
        }

        for(int i=0; i<=snake.size(); i++)
            if(x_comida == snake[i].x && y_comida == snake[i].y) continue;

    }

}

void recoloca_comida()
{
	readimagefile(comida, x_comida, y_comida, x_comida+20, y_comida+20);
}

void gameover()
{
	string line;

	readimagefile(game_over,max_x/2-170,max_y/2-25,max_x/2+170,max_y/2+25);//imprime gameover na tela
	cout << "\nYou've scored " << (snake.size()-2)*10*speed << " points!\n\n";

	saveHighscore();

	system("PAUSE");
	exit(0);//encerra o programa
}

void ponto()
{
	bgiout << "Score: " << (snake.size()-2)*10*speed ;//imprime
	outstreamxy( 20, max_y-19); //coloca na posi��o
}

void move_cobra()
{
    readimagefile(corpo,snake[0].x,snake[0].y,snake[0].x+20,snake[0].y+20);

    snake[snake.size()-1] = (cobra){0,0};

    for(int i=snake.size()-1; i>0; i--)
	{
		struct cobra aux = snake[i];
		snake[i] = snake[i - 1];
		snake[i - 1] = aux;
	}

	snake[0] = (cobra){x,y};

	readimagefile(tela,snake[snake.size()-1].x,snake[snake.size()-1].y,
					  snake[snake.size()-1].x+20,snake[snake.size()-1].y+20);
}

void bordas()
{
	// x e y iniciais || x e y finais
    readimagefile(borda, 10, 10, max_x-10, 20); //borda superior
    readimagefile(borda, 10, max_y-19, max_x-10, max_y-10); //borda inferior
    readimagefile(borda, 10, 10, 19, max_y-10); //borda direita
    readimagefile(borda, max_x-19, 19, max_x-10, max_y-10); //borda esquerda
}

int main( )
{
	short x1, y1;

	cout << "What's your name? ";
	cin >> nome;

	cout << "Enter speed (1-3):";
	cin >> speed;

	initwindow( max_x , max_y , "Snake" );//inicia a janela

	snake.push_back( (cobra){ 0, 0} );
	snake.push_back( (cobra){ 0, 0} );

	bordas();

	coloca_comida();
	ponto();

	for ever//loop principal
    {
		if(x == x_comida && y == y_comida)
		{
			snake.push_back( (cobra){ 0, 0} );
			ponto();
			coloca_comida();
		}

		if(!verifica_pos()) gameover();

		if(kbhit())//se alguma tecla foi pressionada
		{
			seta = getch();//pega o valor no buffer do teclado
			if(seta == UP && dir == DOWN)
	        {
	            seta = DOWN;
	        }
	        else if(seta == DOWN && dir == UP)
	        {
	            seta = UP;
	        }
	        else if(seta == LEFT && dir == RIGHT)
	        {
	        	seta = RIGHT;
	        }
	        else if(seta == RIGHT && dir == LEFT)
	        {
	            seta = LEFT;
	        }
		}

        if(seta == ESC) break;
        else if(seta == UP && dir != DOWN)
        {
			y-=20;
            dir = seta;
        }
        else if(seta == DOWN && dir != UP)
        {
			y+=20;
            dir = seta;
        }
        else if(seta == LEFT && dir != RIGHT)
        {
			x-=20;
        	dir = seta;
        }
        else if(seta == RIGHT && dir != LEFT)
        {
			x+=20;
            dir = seta;
        }
        else seta = dir;

        move_cobra();


		switch (dir)//imprime a cabe�a na dire��o correta
        {
            case UP:
                readimagefile(head_up, x, y, x+20, y+20);
                break;

            case DOWN:
                readimagefile(head_down, x, y, x+20, y+20);
                break;

            case RIGHT:
                readimagefile(head_right, x, y, x+20, y+20);
                break;

            case LEFT:
                readimagefile(head_left, x, y, x+20, y+20);
                break;
        }

		recoloca_comida();

		delay(120/speed);
    }

	closegraph( );//fecha a janela

	return( 0 );
}
